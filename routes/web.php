<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/auth/token/{token}', "Auth\LoginController@authenticate");

Route::get('/dashboard', function () {
    return auth()->user()->name;
})->middleware('auth');

Route::get('/logout', "Auth\LoginController@logout")->middleware('auth');
