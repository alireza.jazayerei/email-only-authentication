<?php

namespace App;

use App\Notifications\SendTokenLink;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class LoginToken extends Model
{
    protected $fillable = ["user_id", "token", "expires_at"];

    public static function generateFor($user)
    {
        return static::create([
            "user_id" => $user->id,
            "token" => Str::random(50),
            "expires_at" => Carbon::now()->addRealMinutes(5),
        ]);
    }

    public function send()
    {
        $this->user->notify(new SendTokenLink($this->token));
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function getRouteKeyName()
    {
        return 'token';
    }
}
