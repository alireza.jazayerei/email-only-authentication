<?php


namespace App\Http\Controllers\Auth;


use App\LoginToken;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthenticatesUsers
{
    /**
     * @var Request
     */
    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function invite()
    {
        $this->validateRequest()
            ->generateToken()
            ->send();
    }

    protected function generateToken()
    {
        $user = User::byEmail($this->request->input('email'));
        return LoginToken::generateFor($user);
    }

    protected function validateRequest()
    {
        $this->request->validate([
            "email" => ["required", "email", "exists:users,email"]
        ]);
        return $this;
    }

    protected function login($token)
    {
        Auth::login($token->user);
        $token->delete();
    }
}
